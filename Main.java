import javax.swing.*;
import java.util.*;
import java.io.File;
import java.awt.event.*;

class Help {
    public static String getPathName(File dirFile, String name) {
        return dirFile.getPath()+"/"+name;
    }

    public static ArrayList<File> getAllFile(File dirFile) {
        ArrayList<File> fs = new ArrayList<File>();
        for(String s : dirFile.list()) {
            File f=new File(Help.getPathName(dirFile,s));
            if(f.isFile())
                fs.add(f); //应排除非图片的
        }
        return fs;
    }

    public static Long getDate(File file) {
        Long lastModified = file.lastModified();
        //Date date = new Date(lastModified);
        return lastModified;
    }
}

class DateComparator implements Comparator<File> {
    @Override
    public int compare(File o1, File o2) {
        return Help.getDate(o1).compareTo(Help.getDate(o2));
    }
}

class LengthComparator implements Comparator<File> {
    @Override
    public int compare(File o1, File o2) {
        Long l1=o1.length();
        Long l2=o2.length();
        return l1.compareTo(l2);
    }
}

class pager {
    private int index;
    private ArrayList<File> allFile;
    private JLabel label;

    public pager(ArrayList<File> allFile, int index,JLabel label) {
        this.index=index;
        this.allFile=allFile;
        this.label=label;
    }

    public void next() {
        index+=1;
        if(index==allFile.size())
            index=0;
        updatePic();
    }

    public void last() {
        index-=1;
        if(index==-1)
            index=allFile.size()-1;
        updatePic();
    }

    public void updatePic() {
        var icon = new ImageIcon(allFile.get(index).getPath());
        label.setIcon(icon);
    }
}

abstract class mouselistener implements MouseListener
{
    pager p;
	public mouselistener(pager p) {
        this.p=p;
    }
	
	public void mousePressed(MouseEvent e){}
	public void mouseExited(MouseEvent e){}
	public void mouseEntered(MouseEvent e){}
	public void mouseClicked(MouseEvent e){}
}


class picWin {
    static public boolean isOpen=false;
    private JLabel label = new JLabel("");

    picWin(ArrayList<File> allFile, int index) {
        // 创建及设置窗口
        JFrame frame = new JFrame("浏览");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 600);
        frame.setLocationRelativeTo(null); //窗体居中显示
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        label.setBounds(0,0,800,600);
        frame.getContentPane().add(label);

        pager p=new pager(allFile,index,label);
        p.updatePic();

        JButton button = new JButton("上一张");
        button.setBounds(820,200,80,25);
        frame.getContentPane().add(button);
        button.addMouseListener(new mouselistener(p) {
            public void mouseReleased(MouseEvent e) {
                p.last();
            }
        });

        JButton button2 = new JButton("下一张");
        button2.setBounds(820,250,80,25);
        frame.getContentPane().add(button2);
        button2.addMouseListener(new mouselistener(p) {
            public void mouseReleased(MouseEvent e) {
                p.next();
            }
        });

        frame.setVisible(true); // 显示窗口 
    }
}


class mainWin {
    private String dirPath;
    private File dirFile;
    private ArrayList<File> allFile;

    private void sortDate() {
        Collections.sort(allFile, new DateComparator());
        updateListModel();
    }

    private void sortLength() {
        Collections.sort(allFile, new LengthComparator());
        updateListModel();
    }

    private void onButtonOk()
	{
        dirPath = textField.getText(); //获取输入内容
        
		dirFile = new File(dirPath);
        if(!dirFile.exists() || dirFile.isFile())
            JOptionPane.showMessageDialog(null,"目录不存在");
        else {
            allFile=Help.getAllFile(dirFile);
            updateListModel();
        }
    }

    private void valueChanged() { // 列表框被选择
        if(picWin.isOpen==false) {
            new picWin(this.allFile,listBox.getSelectedIndex());
            picWin.isOpen=true;
        }
        else
            picWin.isOpen=false;
    }

    private void updateListModel() {
        listModel.clear();
        for(File f : allFile) {
            listModel.addElement(f.getName());
        }
    }

    private DefaultListModel<String> listModel = new DefaultListModel<String>();
    private JList<String> listBox = new JList<String>(listModel);
    private JTextField textField = new JTextField(50);

    mainWin() {
        // 创建及设置窗口
        JFrame frame = new JFrame("图片浏览器");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 440);
        frame.setLocationRelativeTo(null); //窗体居中显示
        frame.getContentPane().setLayout(null);

        JButton button = new JButton("确定");
        button.setBounds(500,20,60,25);
        textField.setBounds(50,20,450,25);
        frame.getContentPane().add(textField);
        frame.getContentPane().add(button);
        button.addActionListener((e) -> {
			this.onButtonOk();
        });

        JButton button2 = new JButton("时间排序");
        JButton button3 = new JButton("大小排序");
        button2.setBounds(130,360,100,25);
        button3.setBounds(330,360,100,25);
        frame.getContentPane().add(button2);
        frame.getContentPane().add(button3);
        button2.addActionListener((e) -> {
			this.sortDate();
        });
        button3.addActionListener((e) -> {
			this.sortLength();
        });
        
        listBox.setBounds(20,50,550,300);
        frame.getContentPane().add(listBox);
        listBox.addListSelectionListener((e) -> {
			this.valueChanged();
        });
        
        frame.setVisible(true); // 显示窗口
    }
}

public class Main {
    private static void createAndShowGUI() {
        new mainWin();
    }

    public static void main(String[] args) {
        // 显示应用 GUI
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}